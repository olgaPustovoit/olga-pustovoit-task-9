document.addEventListener('scroll', function(event) {
    var element = event.target;

  });

const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        rawData = data.results;
        console.log(rawData);
        const contentElement = document.getElementById("contentId");
        const emptyCharacter = document.getElementsByClassName("character")[0];

        rawData.forEach(character => {
            const node = emptyCharacter.cloneNode(true);
            createCharacter(node, character, rawData);

            contentElement.appendChild(node);
        });

        //remove empty node
        contentElement.children[0].remove();

        hideItems(contentElement.children);
    })

    .catch((error) => {
        console.log(JSON.stringify(error));
    });

function createCharacter(element, character, characters) {
    element.setAttribute("data-id", character.id);
    element.children[0].innerHTML = `<span>Name: </span><span>${character.name}</span>`;
    element.children[1].src = character.image;
    element.children[2].innerHTML = `<span>Species: </span><span>${character.species}</span>`;
    element.children[3].innerHTML = `<span>Location: </span><span>${character.location.name}</span>`;
    element.children[4].innerHTML = `<span>Created date: </span><span>${new Date(character.created).toDateString()}</span>`;
    element.children[5].innerHTML = `<span>Episodes: </span><span>${getEpisodes(character.episode)}</span>`;
    element.getElementsByTagName('button')[0].addEventListener("click", removeCard);

}

function getEpisodes(episodes) {
    const episodesNumber = episodes.map(episod => episod.slice(40));

    return episodesNumber.join(', ');
}

function removeCard(event) {
    const id = event.path[1].attributes[1].value;
    event.path[1].remove();
}

function hideItems(nodes) {
    for (let i = 0; i < nodes.length; i++) {
        if (i >= 10) {
            // nodes.item(i).style.visibility = 'hidden';
            nodes.item(i).style.display = 'none';
        }
    }
}